package ee.upmind.adr;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import ee.upmind.adr.exception.AdrException;
import ee.upmind.adr.models.*;
import ee.upmind.adr.models.pp.AtpDocument;
import ee.upmind.adr.models.pp.Kustutamine;
import ee.upmind.adr.models.pp.Teemad;
import ee.upmind.adr.rs.AdrConnector;
import ee.upmind.adr.rs.AdrConnectorBuilder;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private final List<String> doneFunctions = new ArrayList<>();
    private final List<String> doneSeries = new ArrayList<>();
    private final List<String> doneVolumes = new ArrayList<>();
    private AdrConnector adr;
    private final Config config;
    public static String appVersion = "-";

    public App(Config config) {
        this.config = config;
    }

    public static void main(String[] args) throws IOException {

        /* Get app version from manifest */
        Enumeration<URL> resources = App.class.getClassLoader().getResources("META-INF/MANIFEST.MF");
        while (resources.hasMoreElements()) {
            try {
                URL url = resources.nextElement();
                Properties mf = new Properties();
                try (InputStream is = url.openStream()) {
                    mf.load(is);
                    if (App.class.getCanonicalName().equals(mf.getProperty("Main-Class"))) {
                        appVersion = mf.getProperty("Implementation-Version");
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* Parse command-line options */
        Options options = new Options();
        Option o = new Option("f", "config", true, "alternate configuration file, e.g. \"test\" (default is application.conf)");
        o.setRequired(false);
        options.addOption(o);
        options.addOption(new Option("d", "debug", false, "Log output at debug level"));
        options.addOption(new Option("t", "trace", false, "Log output at trace level (including actual requests and responses)"));
        options.addOption(new Option(null, "clean", false, "Delete everything from ADR"));

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            HelpFormatter help = new HelpFormatter();
            System.out.println("AdrLink version " + appVersion);
            System.out.println(e.getMessage());
            help.printHelp("java -jar AdrLink.jar [<option>]", options);
            System.exit(1);
        }

        Config conf;
        if (cmd.hasOption("f")) {
            logger.info("Using configuration \"" + cmd.getOptionValue("f") + "\"");
            conf = ConfigFactory.load(cmd.getOptionValue("f"));
        } else {
            conf = ConfigFactory.load();
        }

        /* Intercept JUL logging and send to where other logs go */
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();

        /* Set logging level of underlying log4j, if command line has the switches */
        if (cmd.hasOption("t")) {
            org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.TRACE);
            logger.info("Logging level set to TRACE");
        } else if (cmd.hasOption("d")) {
            org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.DEBUG);
            logger.info("Logging level set to DEBUG");
        }

        /* Now get to work */
        App app = new App(conf);
        if (cmd.hasOption("clean")) {
            app.clean();
        } else {
            app.work();
        }
    }

    public void clean() {
        logger.warn("Removing everything from ADR");
        try {
            adr = AdrConnectorBuilder.newBuilder()
                    .withEndpointUrl(config.getString("endpointUri"))
                    .withTrustStorePath(config.getString("trustStorePath"))
                    .withTrustStorePass(config.getString("trustStorePass"))
                    .build();
            adr.deleteDocuments();
            logger.info("All documents removed from ADR");
            adr.deleteDocumentTypes();
            logger.info("Document types removed from ADR");
            adr.deleteFunctions();
            logger.info("Functions removed from ADR");
        } catch (AdrException e) {
            logger.error("Cleaning ADR database failed", e);
        }
    }
    public void work() {
        Statistics stats = new Statistics();

        try {
            logger.info("Start publishing documents to ADR");

            adr = AdrConnectorBuilder.newBuilder()
                    .withEndpointUrl(config.getString("endpointUri"))
                    .withTrustStorePath(config.getString("trustStorePath"))
                    .withTrustStorePass(config.getString("trustStorePass"))
                    .build();

            /* Update document types from config to ADR */
            for (Map.Entry<String, ConfigValue> entry : config.getObject("adrDocTypes").entrySet()) {
                DocumentType dt = new DocumentType(entry.getKey(), entry.getValue().unwrapped().toString());
                logger.debug("Updating document type: " + dt.getName() + " -> " + dt.getTitle());
                adr.updateDocumentType(dt);
            }

            /* Process XML files */
            Path inputPath = Paths.get(config.getString("inputPath"));
            logger.info("Input path " + inputPath.toAbsolutePath().toString());
            Path failedPath = Paths.get(config.getString("failedPath"));

            JAXBContext jaxbContext = JAXBContext.newInstance("ee.upmind.adr.models.pp");
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            logger.debug("Processing files from directory " + inputPath.toAbsolutePath().toString());
            Files.list(inputPath)
                    .filter(f -> f.toString().toLowerCase().endsWith(".xml"))
                    .sorted()
                    .forEach(path -> {
                try {
                    logger.info("File: " + path.toString());
                    Object atpObject = unmarshaller.unmarshal(path.toFile());

                    if (atpObject instanceof AtpDocument) {
                        updateDocument((AtpDocument) atpObject);
                        stats.updated();

                    } else if (atpObject instanceof Kustutamine) {
                        deleteDocument((Kustutamine) atpObject);
                        stats.deleted();

                    } else if (atpObject instanceof Teemad) {
                        updateSubjects((Teemad) atpObject);
                    }

                    if (!config.getBoolean("keepFiles"))
                        Files.delete(path);

                } catch (Exception e) {
                    logger.error("Failed processing file " + path.toString(), e);
                    stats.failed();
                    try {
                        Files.move(path, failedPath.resolve(path.getFileName()), StandardCopyOption.REPLACE_EXISTING);

                        //TODO: Move attachments by the file name suffix
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            });

        } catch (AdrException | JAXBException | IOException e) {
            e.printStackTrace();
            stats.failed();
        } finally {
            logger.info("Done publishing documents to ADR, documents updated " + stats.getUpdated() + ", deleted " + stats.getDeleted() + ", failed " + stats.getFailed());
            if (stats.getFailed() == 0) {
                sendMessage("onsuccess", stats);
            } else {
                sendMessage("onfailure", stats);
            }
        }
    }

    private void updateDocument(AtpDocument atpDoc) throws AdrException, IOException {

        /* Verify mandatory data */
        if (atpDoc.getDokNr() == null || "".equals(atpDoc.getDokNr()))
            throw new AdrException("Document has no registration number");

        if (atpDoc.getRegistreerimiskuupaev() == null)
            throw new AdrException("Document has no registration date");

        if (atpDoc.getLiik() == null || "".equals(atpDoc.getLiik()))
            throw new AdrException("Document has no type");

        /* Convert data from ATP to ADR format */
        Document adrDoc = new Document(atpDoc);

        if (logger.isDebugEnabled()) {
            logger.debug("Updating document " + adrDoc.getNoderef() + ": " + adrDoc.getTitle());
        }
        /* Remap document type */
        String docTypeKey = "ppDocTypes.\"" + (atpDoc.getLiik() + atpDoc.getSuund()).replaceAll("\\.", "") + "\"";
        String docTypeName = config.getString(docTypeKey);
        logger.debug("Document type: " + docTypeName + " -> " + config.getString("adrDocTypes." + docTypeName));
        adrDoc.setDocTypeName(docTypeName);

        /* Update document function, if not already processed */
        Function f = new Function(atpDoc);
        if (f.isValid() && !doneFunctions.contains(f.getNoderef())) {
            logger.debug("Updating function: " + f.getNoderef());
            adr.updateFunction(f);
            doneFunctions.add(f.getNoderef());
        }

        /* Update document series, if not already processed */
        Series s = new Series(atpDoc);
        s.setParentFunction(f);
        if (s.isValid() && !doneSeries.contains(s.getNoderef())) {
            logger.debug("Updating series: " + s.getNoderef());
            adr.updateSeries(s);
            doneSeries.add(s.getNoderef());
        }

        /* Update doc volume */
        Volume v = new Volume(atpDoc);
        v.setParentSeries(s);
        if (!doneVolumes.contains(v.getNoderef())) {
            logger.debug("Updating volume: " + v.getNoderef());
            adr.updateVolume(v);
            doneVolumes.add(v.getNoderef());
        }

        /* Update document */
        logger.debug("Updating document data");
        adrDoc.setParentVolume(v);
        adr.updateDocument(adrDoc);

        /* File attachments */
        logger.debug("Deleting existing files from document (if any)");
        adr.deleteFiles(adrDoc);
        String strAtts = atpDoc.getViide();
        if (strAtts != null && !"".equals(strAtts)) {
            logger.debug("Updating document attachments");
            Map<String, File> files = new HashMap<>();
            for (String fileName : strAtts.split(",")) {
                files.put(fileName, new File(config.getString("inputPath"), fileName + "." + adrDoc.getNoderef()));
            }
            adr.createFiles(adrDoc, files);
            if (!config.getBoolean("keepFiles")) {
                for (File file : files.values()) {
                    if (Files.exists(file.toPath()))
                        Files.delete(file.toPath());
                }
            }
        }

        /* Document association with parent */
        if (!"".equals(atpDoc.getAlgDokNr()) && !atpDoc.getDokNr().equals(atpDoc.getAlgDokNr())) {
            logger.debug("Updating document associations");
            adr.createAssociation(atpDoc.getAlgDokNr(), atpDoc.getDokNr(), Reference.REPLY);
        } else {
            logger.debug("Document has no associations");
        }

    }

    private void deleteDocument(Kustutamine doc) throws AdrException {
        logger.debug("Delete document: " + doc.getDokNr());
        Document d = new Document();
        d.setNoderef(doc.getDokNr());
        adr.deleteDocument(d);
    }

    private void updateSubjects(Teemad subs) {
        //TODO: anything useful?
        logger.debug("Subjects XML");
    }

    private void sendMessage(String type, Statistics stats) {

        if (!config.hasPath("mailer.host"))
            return;

        if (!config.hasPath("messages." + type)) {
            logger.debug(type + " message not defined, skipping");
        } else {
            logger.debug("Sending " + type + " message");
        }

        Properties mailProps = new Properties();
        Authenticator authenticator = null;

        mailProps.setProperty("mail.smtp.host", config.getString("mailer.host"));
        if (config.hasPath("mailer.port"))
            mailProps.setProperty("mail.smtp.port", config.getString("mailer.port"));

        if (config.hasPath("mailer.user")) {
            mailProps.setProperty("mail.smtp.auth", "true");
            authenticator = new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(config.getString("mailer.user"), config.getString("mailer.password"));
                }
            };
        }

        if (logger.isDebugEnabled())
            mailProps.setProperty("mail.debug", "true");

        Session session = Session.getDefaultInstance(mailProps, authenticator);

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(config.getString("mailer.from")));
            if (config.hasPath("mailer.to"))
                message.setRecipients(Message.RecipientType.TO, config.getString("mailer.to"));
            if (config.hasPath("messages." + type + ".to"))
                message.addRecipients(Message.RecipientType.TO, config.getString("messages." + type + ".to"));
            message.setSubject(config.getString("messages." + type + ".subject"));
            message.setText(String.format(config.getString("messages." + type + ".body"), stats.getUpdated(), stats.getDeleted(), stats.getFailed()));
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

}

