package ee.upmind.adr.exception;

public class AdrException extends Exception {
    public AdrException(String message) { super(message); }
    public AdrException(String message, Throwable cause) {
        super(message, cause);
    }
}
