package ee.upmind.adr.rs;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.upmind.adr.exception.AdrException;
import ee.upmind.adr.models.*;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.*;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class AdrRestConnector implements AdrConnector {
    private static final Logger logger = LoggerFactory.getLogger(AdrRestConnector.class);
    private final String endpointUrl;
    private final Configuration clientConfig;
    private final KeyStore trustStore;
    private ObjectMapper mapper;
    private Client client;

    public AdrRestConnector(AdrConnectorBuilder builder) {
        endpointUrl = builder.endpointUrl;
        clientConfig = builder.clientConfig;
        trustStore = builder.trustStore;
    }

    @Override
    public boolean hasDocument(Document adrDoc) throws AdrException {
        try (Response r = getClient(docPath(adrDoc)).head()) {
            switch (r.getStatus()) {
                case 200: return true;
                case 404: return false;
                default:
                    throw new AdrException("Failure checking doc existence, result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void createDocument(Document adrDoc) throws AdrException {
        /* Send document data */
        Entity<Document> docEntity = Entity.entity(adrDoc, MediaType.APPLICATION_JSON);
        String uri = docPath(null);
        logRequest("POST " + uri, adrDoc);
        try (Response r = getClient(uri).post(docEntity)) {
            if (r.getStatus() != 200 && r.getStatus() != 201) {
                logger.error("Document update result: " + r.getStatus() + ": " + r.toString());
                logger.error(r.readEntity(String.class));
                throw new AdrException("Document update failed with result: " + r.getStatus() + ": " + r.toString());
            } else {
                logger.debug("Document update result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void updateDocument(Document adrDoc) throws AdrException {
        /* Send document data */
        Entity<Document> docEntity = Entity.entity(adrDoc, MediaType.APPLICATION_JSON);
        String uri = docPath(adrDoc);
        logRequest("PUT " + uri, adrDoc);
        try (Response r = getClient(uri).put(docEntity)) {
            if (r.getStatus() != 200 && r.getStatus() != 201) {
                logger.error("Document update result: " + r.getStatus() + ": " + r.toString());
                logger.error(r.readEntity(String.class));
                throw new AdrException("Document update failed with result: " + r.getStatus() + ": " + r.toString());
            } else {
                logger.debug("Document update result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void deleteDocument(Document adrDoc) throws AdrException {
        try (Response r = getClient(docPath(adrDoc)).delete()) {
            if (r.getStatus() != 404 && r.getStatus() != 204) {
                logger.error(r.toString());
                throw new AdrException("Document removal failed with result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void deleteDocuments() throws AdrException {
        try (Response r = getClient(docPath(null)).delete()) {
            if (r.getStatus() != 404 && r.getStatus() != 204) {
                logger.error(r.toString());
                throw new AdrException("Documents removal failed with result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    public List<Function> getFunctions() throws AdrException {
        try (Response r = getClient(functionPath(null)).get()) {
            return r.readEntity(new GenericType<List<Function>>() {});
        }
    }

    @Override
    public boolean hasFunction(Function f) throws AdrException {
        try (Response r = getClient(functionPath(f)).head()) {
            switch (r.getStatus()) {
                case 200: return true;
                case 404: return false;
                default:
                    throw new AdrException("Failure checking function existence, result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void createFunction(Function f) throws AdrException {
        Entity<Function> functionEntity = Entity.entity(f, MediaType.APPLICATION_JSON);
        String uri = functionPath(null);
        logRequest("POST " + uri, f);
        try (Response r = getClient(uri).post(functionEntity)) {
            //if (r.getStatus() != 200 && r.getStatus() != 201) {
            if (r.getStatus() != 201) {
                logger.error("Function create result: " + r.getStatus() + ": " + r.toString());
                if (r.hasEntity())
                    logger.error(r.readEntity(String.class));
                throw new AdrException("Function create failed with result: " + r.getStatus() + ": " + r.toString());
            } else {
                logger.debug("Function create result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void updateFunction(Function f) throws AdrException {
        if (!hasFunction(f)) {
            createFunction(f);
        } else {
            Entity<Function> functionEntity = Entity.entity(f, MediaType.APPLICATION_JSON);
            String uri = functionPath(f);
            logRequest("PUT " + uri, f);
            try (Response r = getClient(uri).put(functionEntity)) {
                //if (r.getStatus() != 200 && r.getStatus() != 201) {
                if (r.getStatus() != 200) {
                    logger.error("Function update result: " + r.getStatus() + ": " + r.toString());
                    if (r.hasEntity())
                        logger.error(r.readEntity(String.class));
                    throw new AdrException("Function update failed with result: " + r.getStatus() + ": " + r.toString());
                } else {
                    logger.debug("Function update result: " + r.getStatus() + ": " + r.toString());
                }
            }
        }
    }

    @Override
    public void deleteFunction(Function f) throws AdrException {
        try (Response r = getClient(functionPath(f)).delete()) {
            if (r.getStatus() != 404 && r.getStatus() != 204) {
                logger.error(r.toString());
                throw new AdrException("Function removal failed with result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void deleteFunctions() throws AdrException {
        try (Response r = getClient(functionPath(null)).delete()) {
            if (r.getStatus() != 404 && r.getStatus() != 204) {
                logger.error(r.toString());
                throw new AdrException("Functions removal failed with result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public boolean hasSeries(Series s) throws AdrException {
        try (Response r = getClient(seriesPath(s.getParentFunction(), s)).head()) {
            switch (r.getStatus()) {
                case 200: return true;
                case 404: return false;
                default:
                    throw new AdrException("Failure checking series existence, result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void createSeries(Series s) throws AdrException {
        Entity<Series> seriesEntity = Entity.entity(s, MediaType.APPLICATION_JSON);
        String uri = seriesPath(s.getParentFunction(), null);
        logRequest("POST " + uri, s);
        try (Response r = getClient(uri).post(seriesEntity)) {
            //if (r.getStatus() != 200 && r.getStatus() != 201) {
            if (r.getStatus() != 201) {
                logger.error("Series create result: " + r.getStatus() + ": " + r.toString());
                logger.error(r.readEntity(String.class));
                throw new AdrException("Series create failed with result: " + r.getStatus() + ": " + r.toString());
            } else {
                logger.debug("Series create result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void updateSeries(Series s) throws AdrException {
        if (!hasSeries(s)) {
            createSeries(s);
        } else {
            Entity<Series> seriesEntity = Entity.entity(s, MediaType.APPLICATION_JSON);
            String uri = seriesPath(s.getParentFunction(), s);
            logRequest("PUT " + uri, s);
            try (Response r = getClient(uri).put(seriesEntity)) {
                //if (r.getStatus() != 200 && r.getStatus() != 201) {
                if (r.getStatus() != 200) {
                    logger.error("Series update result: " + r.getStatus() + ": " + r.toString());
                    logger.error(r.readEntity(String.class));
                    throw new AdrException("Series update failed with result: " + r.getStatus() + ": " + r.toString());
                } else {
                    logger.debug("Series update result: " + r.getStatus() + ": " + r.toString());
                }
            }
        }
    }

    @Override
    public void deleteSeries(Series s) throws AdrException {
        try (Response r = getClient(seriesPath(s.getParentFunction(), s)).delete()) {
            if (r.getStatus() != 404 && r.getStatus() != 204) {
                logger.error(r.toString());
                throw new AdrException("Series removal failed with result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public List<Series> getSeries(Function f) throws AdrException {
        try (Response r = getClient(seriesPath(f, null)).get()) {
            return r.readEntity(new GenericType<List<Series>>() {});
        }
    }

    @Override
    public void deleteSeries(Function f) throws AdrException {
        try (Response r = getClient(seriesPath(f, null)).delete()) {
            if (r.getStatus() != 404 && r.getStatus() != 204) {
                logger.error(r.toString());
                throw new AdrException("Series removal failed with result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public boolean hasVolume(Volume v) throws AdrException {
        try (Response r = getClient(volumePath(v.getParentSeries(), v)).head()) {
            switch (r.getStatus()) {
                case 200: return true;
                case 404: return false;
                default:
                    throw new AdrException("Failure checking volume existence, result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void createVolume(Volume v) throws AdrException {
        Entity<Volume> volumeEntity = Entity.entity(v, MediaType.APPLICATION_JSON);
        String uri = volumePath(v.getParentSeries(), null);
        logRequest("POST " + uri, v);
        try (Response r = getClient(uri).post(volumeEntity)) {
            //if (r.getStatus() != 200 && r.getStatus() != 201) {
            if (r.getStatus() != 201) {
                logger.error("Volume create result: " + r.getStatus() + ": " + r.toString());
                logger.error(r.readEntity(String.class));
                throw new AdrException("Volume create failed with result: " + r.getStatus() + ": " + r.toString());
            } else {
                logger.debug("Volume create result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void updateVolume(Volume v) throws AdrException {
        if (!hasVolume(v)) {
            createVolume(v);
        } else {
            Entity<Volume> volumeEntity = Entity.entity(v, MediaType.APPLICATION_JSON);
            String uri = volumePath(v.getParentSeries(), v);
            logRequest("PUT " + uri, v);
            try (Response r = getClient(uri).put(volumeEntity)) {
                //if (r.getStatus() != 200 && r.getStatus() != 201) {
                if (r.getStatus() != 200) {
                    logger.error("Volume update result: " + r.getStatus() + ": " + r.toString());
                    logger.error(r.readEntity(String.class));
                    throw new AdrException("Volume update failed with result: " + r.getStatus() + ": " + r.toString());
                } else {
                    logger.debug("Volume update result: " + r.getStatus() + ": " + r.toString());
                }
            }
        }
    }

    @Override
    public void deleteVolume(Volume v) throws AdrException {
        try (Response r = getClient(volumePath(v.getParentSeries(), v)).delete()) {
            if (r.getStatus() != 404 && r.getStatus() != 204) {
                logger.error(r.toString());
                throw new AdrException("Volume removal failed with result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public List<Volume> getVolumes(Series s) throws AdrException {
        try (Response r = getClient(volumePath(s, null)).get()) {
            return r.readEntity(new GenericType<List<Volume>>() {});
        }
    }

    @Override
    public void deleteVolumes(Series s) throws AdrException {
        try (Response r = getClient(volumePath(s, null)).delete()) {
            if (r.getStatus() != 404 && r.getStatus() != 204) {
                logger.error(r.toString());
                throw new AdrException("Volumes removal failed with result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void updateDocumentType(DocumentType dt) throws AdrException {
        Entity<DocumentType> requestEntity = Entity.entity(dt, MediaType.APPLICATION_JSON);
        String uri = docTypePath(dt);
        logRequest("PUT " + uri, dt);
        try (Response r = getClient(uri).put(requestEntity)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Result: " + r.getStatus() + ": " + r.toString());
            }
            if (r.getStatus() != 200 && r.getStatus() != 201) {
                logger.error("DocType update result: " + r.getStatus() + ": " + r.toString());
                logger.error(r.readEntity(String.class));
                throw new AdrException("DocType update failed with result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void deleteDocumentTypes() throws AdrException {
        try (Response r = getClient(docTypePath(null)).delete()) {
            if (r.getStatus() != 404 && r.getStatus() != 204) {
                logger.error(r.toString());
                throw new AdrException("Document types removal failed with result: " + r.getStatus() + ": " + r.toString());
            }
        }
    }

    @Override
    public void deleteFiles(Document doc) throws AdrException {
        String uri = filesPath(doc);
        logRequest("DELETE " + uri, null);
        try (Response r = getClient(uri).delete()) {
            logger.debug("Delete files result: " + r.getStatus() + ": " + r.toString());
        }
    }

    @Override
    public void createFiles(Document doc, Map<String, File> files) throws AdrException {
        MultiPart multiPart = new FormDataMultiPart();
        for (String fileName : files.keySet()) {
            File file = files.get(fileName);
            try {
                multiPart.bodyPart(new StreamDataBodyPart("files", new FileInputStream(file), fileName));
            } catch (FileNotFoundException e) {
                throw new AdrException("Dokumendis viidatud faili " + fileName + " ei leitud", e);
            }
        }
        Entity<MultiPart> mpEntity = Entity.entity(multiPart, multiPart.getMediaType());
        String uri = filesPath(doc);
        logRequest("POST " + uri, null);
        try (Response r = getClient(uri).post(mpEntity)) {
            logger.debug("Files creation result: " + r.getStatus() + ": " + r.toString());
        }
    }

    public void createAssociation(String sourceNodeRef, String targetNodeRef, String targetType) throws AdrException {
        Reference ref = new Reference();
        ref.setTargetDocumentNoderef(targetNodeRef);
        ref.setType(targetType);
        Document parent = new Document();
        parent.setNoderef(sourceNodeRef);
        String uri = referencePath(parent, ref);
        Entity<Reference> refEntity = Entity.entity(ref, MediaType.APPLICATION_JSON);
        logRequest("PUT " + uri, ref);
        try (Response r = getClient(uri).put(refEntity)) {
            logger.debug("Association update result: " + r.getStatus() + ": " + r.toString());
        }
    }

    private Invocation.Builder getClient(String strUri) throws AdrException {
        URI uri = null;
        try {
            uri = new URI(strUri);
        } catch (URISyntaxException e) {
            throw new AdrException("Exception encoding path '" + strUri + "'", e);
        }
        if (client == null) {
            ClientBuilder builder = ClientBuilder.newBuilder().withConfig(clientConfig);
            if (trustStore != null) {
                builder.trustStore(trustStore);
            }
            client = builder.build();
            if (logger.isTraceEnabled()) {
                Feature feature = new LoggingFeature(
                        java.util.logging.Logger.getLogger(getClass().getName()),
                        Level.FINE,
                        LoggingFeature.Verbosity.PAYLOAD_TEXT,
                        1024);
                client.register(feature);
            }
        }
        return client.target(uri).request().accept(MediaType.APPLICATION_JSON_TYPE);
    }

    private String docTypePath(DocumentType t) throws AdrException {
        return endpointUrl + "/document-types/" + (t == null ? "" : enc(t.getName()));
    }
    private String functionPath(Function f) {
        return endpointUrl + "/functions/" + (f == null ? "" : (b64enc(f.getMark()) + "/" + b64enc(f.getTitle())));
    }
    private String seriesPath(Function f, Series s) {
        return functionPath(f) + "/series/" + (s == null ? "" : (b64enc(s.getMark()) + "/" + b64enc(s.getTitle())));
    }
    private String volumePath(Series s, Volume v) {
        return seriesPath(s.getParentFunction(), s) + "/volumes/" + (v == null ? "" : b64enc(v.getNoderef()));
    }
    private String docPath(Document d) {
        return endpointUrl + "/documents/" + (d == null ? "" : b64enc(d.getNoderef()));
    }
    private String referencePath(Document d, Reference r) {
        return docPath(d) + "/document-associations/" + (r == null ? "" : b64enc(r.getTargetDocumentNoderef()));
    }
    private String filesPath(Document d) {
        return docPath(d) + "/files/";
    }

    private static String enc(String s) throws AdrException {
        try {
            return URLEncoder.encode(s, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            throw new AdrException("Exception encoding path '" + s + "'", e);
        }
    }
    private static String b64enc(String s) { return DatatypeConverter.printBase64Binary(s.getBytes(StandardCharsets.UTF_8)); }

    private void logRequest(String uri, Object o) {
        if (logger.isDebugEnabled()) {
            logger.debug(uri);
            /*if (o != null) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    logger.debug(mapper.writeValueAsString(o));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }*/
        }
    }
}
