package ee.upmind.adr.rs;

import ee.upmind.adr.exception.AdrException;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.glassfish.jersey.client.RequestEntityProcessing;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.ws.rs.core.Feature;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class AdrConnectorBuilder {
    String endpointUrl;
    ClientConfig clientConfig;
    SSLContext sslContext;
    KeyStore trustStore;
    String trustStorePath;
    String trustStorePass = "changeit";

    private static final Logger logger = LoggerFactory.getLogger(AdrConnectorBuilder.class);

    public AdrConnectorBuilder withEndpointUrl(String endpointUrl) {
        this.endpointUrl = endpointUrl;
        return this;
    }

    public AdrConnectorBuilder withClientConfig(ClientConfig clientConfig) {
        this.clientConfig = clientConfig;
        return this;
    }

    public AdrConnectorBuilder withSslContext(SSLContext sslContext) {
        this.sslContext = sslContext;
        return this;
    }

    public AdrConnectorBuilder withTrustStorePath(String tksPath) {
        this.trustStorePath = tksPath;
        return this;
    }

    public AdrConnectorBuilder withTrustStorePass(String tksPass) {
        this.trustStorePass = tksPass;
        return this;
    }

    public AdrConnector build() throws AdrException {
        if (this.clientConfig == null) {
            clientConfig = new ClientConfig();
            clientConfig.property(ClientProperties.REQUEST_ENTITY_PROCESSING, RequestEntityProcessing.CHUNKED.name());
            clientConfig.property(HttpUrlConnectorProvider.USE_FIXED_LENGTH_STREAMING, true);
            clientConfig.register(MultiPartFeature.class);
            if (LoggerFactory.getLogger(AdrRestConnector.class).isTraceEnabled()) {
                Feature feature = new LoggingFeature(
                        java.util.logging.Logger.getLogger(AdrRestConnector.class.getName()),
                        java.util.logging.Level.INFO,
                        LoggingFeature.Verbosity.PAYLOAD_TEXT,
                        1024);
                clientConfig.register(feature);
            }
        }
        if (this.trustStore == null && this.trustStorePath != null) {
            File f = new File(this.trustStorePath);
            try (FileInputStream fin = new FileInputStream(f)) {
                trustStore = KeyStore.getInstance("JKS");
                trustStore.load(fin, trustStorePass.toCharArray());
            } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException e) {
                throw new AdrException("Could not load truststore from " + f.getAbsolutePath(), e);
            }
        }
        return new AdrRestConnector(this);
    }

    public static AdrConnectorBuilder newBuilder() {
        return new AdrConnectorBuilder();
    }

}
