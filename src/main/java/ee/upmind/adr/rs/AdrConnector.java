package ee.upmind.adr.rs;

import ee.upmind.adr.exception.AdrException;
import ee.upmind.adr.models.*;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface AdrConnector {

    /** Verify if the given Document is already in ADR
     * @param d Document to check. Only nodeRef is required to be filled */
    boolean hasDocument(Document d) throws AdrException;

    /** Create a new Document in ADR
     * @param d Document to create */
    void createDocument(Document d) throws AdrException;

    /** Update existing Document in ADR
     * @param d Document to update. If the document does not yet exist, it is created */
    void updateDocument(Document d) throws AdrException;

    /** Delete given Document from ADR. Files are automatically deleted too
     * @param d Document to delete. Only nodeRef is required to be filled */
    void deleteDocument(Document d) throws AdrException;

    /** Delete all documents from ADR! */
    void deleteDocuments() throws AdrException;

    /** Check if given Function exists in ADR
     * @param f Function to check
     * @return true if this Function exists */
    boolean hasFunction(Function f) throws AdrException;

    /** Create new Function in ADR
     * @param f Function to create */
    void createFunction(Function f) throws AdrException;

    /** Update existing Function in ADR. If the Function does not yet exist, it is created
     * @param f Function to update */
    void updateFunction(Function f) throws AdrException;

    /** Delete Function from ADR. Deletion is recursive, all series under this Function are also deleted
     * @param f Function to delete */
    void deleteFunction(Function f) throws AdrException;

    /** Get all Functions from ADR */
    List<Function> getFunctions() throws AdrException;

    /** Delete all functions from ADR! Deletion is recursive, all series under this Function (and all Volumes under the series) are also deleted */
    void deleteFunctions() throws AdrException;

    /** Verify if a Series is already in ADR
     * @param s Series to check
     * @return true if this Series exists */
    boolean hasSeries(Series s) throws AdrException;

    /** Create new Series in ADR.
     * @param s Series to create. Must have parent Function set */
    void createSeries(Series s) throws AdrException;

    /** Update Series in ADR. If the Series does not yet exist, it is created
     * @param s Series to update. Must have parent Function set  */
    void updateSeries(Series s) throws AdrException;

    /** Delete Series from ADR. Deletion is recursive, all Volumes under this Series are also deleted
     * @param s Series to delete */
    void deleteSeries(Series s) throws AdrException;

    /** Get all Series under this Function
     * @param f Function whose children to return */
    List<Series> getSeries(Function f) throws AdrException;

    /** Delete all Series under this Function. Deletion is recursive, all Volumes under these Series are also deleted
     * @param f Function whose children Series to delete */
    void deleteSeries(Function f) throws AdrException;

    /** Check if this Volume is already in ADR
     * @param v Volume to check
     * @return true if this Volume exists */
    boolean hasVolume(Volume v) throws AdrException;

    /** Create new Volume in ADR.
     * @param v Volume to create. Must have parent Series set */
    void createVolume(Volume v) throws AdrException;

    /** Update Volume in ADR. If the Series does not yet exist, it is created
     * @param v Volume to update. Must have parent Series set */
    void updateVolume(Volume v) throws AdrException;

    /** Delete Volume from ADR
     * @param v Volume to delete */
    void deleteVolume(Volume v) throws AdrException;

    /** Get all Volumes under this Series
     * @param s Series whose children to return */
    List<Volume> getVolumes(Series s) throws AdrException;

    /** Delete all Volumes under this Series
     * @param s Series whose children Volumes to delete */
    void deleteVolumes(Series s) throws AdrException;

    /** Update Document type in ADR. If it does not yet exist, it is created
     * @param dt DocumentType to update */
    void updateDocumentType(DocumentType dt) throws AdrException;

    /** Delete Document type from ADR
     * @param dt DocumentType to delete. Deletion is not possible if any documents in ADR reference this type */
    void deleteDocumentTypes() throws AdrException;

    /** Delete all files from this Document
      * @param doc Document whose files to remove */
    void deleteFiles(Document doc) throws AdrException;

    /** Create files for this Document
     * @param doc Document whose files to create
     * @param files List pof files to create */
    void createFiles(Document doc, Map<String, File> files) throws AdrException;

    /** Create association between two Documents in ADR
     * @param sourceNodeRef Parent document if this is a parent-response relationship (REPLY or FOLLOWUP)
     * @param targetNodeRef Response document if this is a parent-response relationship (REPLY or FOLLOWUP)
     * @param targetType Relationship type, REPLY, FOLLOWUP or DEFAULT
     * @throws AdrException
     */
    void createAssociation(String sourceNodeRef, String targetNodeRef, String targetType) throws AdrException;
}
