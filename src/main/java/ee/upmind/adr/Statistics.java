package ee.upmind.adr;

public class Statistics {
    private long updated;
    private long deleted;
    private long failed;

    public void updated() { this.updated++; }
    public long getUpdated() { return this.updated; }
    public void deleted() { this.deleted++; }
    public long getDeleted() { return  this.deleted; }
    public void failed() { this.failed++; }
    public long getFailed() { return this.failed; }
}
