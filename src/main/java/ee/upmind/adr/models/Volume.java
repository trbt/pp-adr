package ee.upmind.adr.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.upmind.adr.models.pp.AtpDocument;

import java.sql.Date;
import java.time.LocalDate;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Volume {
    private String noderef;
    private String mark = "-";
    private String title = "";

    private Date validFromDate = new Date(0);
    private Date validToDate = Date.valueOf(LocalDate.of(3000, 12, 31));

    @JsonIgnore
    private Series parentSeries;

    public Volume(AtpDocument doc) {
        // Toimiku markeering: 6019-2 (indeksi alusel. Kõik, mis jääb pärast kaldkriipsu)
        // Toimiku nimetus: pole vajalik ja ei kohaldu
        if (doc.getIndeks() != null && !"".equals(doc.getIndeks())) {
            String[] parts = doc.getIndeks().split("[\\/]");
            if (parts.length > 1)
                mark = parts[1];
        }
    }

    public String getNoderef() {
        if (this.noderef == null) {
            Series s = this.parentSeries;
            Function f = s.getParentFunction();
            this.noderef = f.getMark() + " " + f.getTitle() + "/" + s.getMark() + " " + s.getTitle() + "/" + getMark();
        }
        return noderef;
    }

    public void setNoderef(String noderef) {
        this.noderef = noderef;
    }

    public String getMark() {
        return mark;
    }
    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public Date getValidFromDate() {
        return validFromDate;
    }
    public void setValidFromDate(Date validFromDate) {
        this.validFromDate = validFromDate;
    }

    public Date getValidToDate() {
        return validToDate;
    }
    public void setValidToDate(Date validToDate) {
        this.validToDate = validToDate;
    }

    public Series getParentSeries() {
        return parentSeries;
    }
    public void setParentSeries(Series s) {
        this.parentSeries = s;
    }
}
