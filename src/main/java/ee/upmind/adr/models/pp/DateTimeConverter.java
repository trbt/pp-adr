package ee.upmind.adr.models.pp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DateFormat;

public class DateTimeConverter {

    private static final String DATE_PATTERN = "dd.MM.yyyy";
    private static final String TIME_PATTERN = "HH:mm:ss";

    public static Date parseDate(String value) {
        try {
            if (value != null && !"".equals(value)) {
                DateFormat df = new SimpleDateFormat(DATE_PATTERN);
                return df.parse(value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String printDate(Date value) {
        if (value != null) {
            DateFormat df = new SimpleDateFormat(DATE_PATTERN);
            return df.format(value);
        }
        return "";
    }

    public static Date parseTime(String value) {
        try {
            if (value != null && !"".equals(value)) {
                DateFormat df = new SimpleDateFormat(TIME_PATTERN);
                return df.parse(value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String printTime(Date value) {
        if (value != null) {
            DateFormat df = new SimpleDateFormat(TIME_PATTERN);
            return df.format(value);
        }
        return "";
    }

}
