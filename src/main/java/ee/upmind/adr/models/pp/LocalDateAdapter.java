package ee.upmind.adr.models.pp;

import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class LocalDateAdapter extends XmlAdapter<String, Date> {
    public Date unmarshal(String value) {
        return DateTimeConverter.parseDate(value);
    }
    public String marshal(Date value) {
        return DateTimeConverter.printDate(value);
    }
}
