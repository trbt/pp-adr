package ee.upmind.adr.models.pp;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Date;

public class LocalTimeAdapter extends XmlAdapter<String, Date> {
    public Date unmarshal(String value) {
        return DateTimeConverter.parseTime(value);
    }
    public String marshal(Date value) {
        return DateTimeConverter.printTime(value);
    }
}
