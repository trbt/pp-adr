package ee.upmind.adr.models;

public class DocumentType {
    private String name;
    private String title;

    public DocumentType(String name, String title) {
        this.name = name; this.title = title;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}
