package ee.upmind.adr.models;

public class Reference {
    /**DEFAULT -- mõlemal pool seose liik "tavaline",
     * REPLY -- target on vastusdokument, source on alusdokument,
     * FOLLOWUP -- target on järgdokument, source on alusdokument */
    private String type;
    private String targetDocumentNoderef;

    public final static String DEFAULT = "DEFAULT";
    public final static String REPLY = "REPLY";
    public final static String FOLLOWUP = "FOLLOWUP";

    public String getType() {return type;}
    public void setType(String type) {this.type = type;}

    public String getTargetDocumentNoderef() {return targetDocumentNoderef;}
    public void setTargetDocumentNoderef(String targetDocumentNoderef) {
        this.targetDocumentNoderef = targetDocumentNoderef;
    }
}
