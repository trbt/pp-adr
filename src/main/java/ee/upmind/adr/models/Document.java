package ee.upmind.adr.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.upmind.adr.models.pp.AtpDocument;

import java.util.Date;
import java.util.Locale;

public class Document {
    private String volumeNodeRef = "";
    private String noderef = "";
    private Date regDateTime;
    private String regNumber = "";
    private String title = "";
    private String accessRestriction = "Avalik";
    private String accessRestrictionReason = "";
    private Date accessRestrictionBeginDate = null;
    private Date accessRestrictionEndDate = null;
    private String accessRestrictionEndDesc = "";
    private Date dueDate = null;
    private Date complianceDate = null;
    private String compilator = "";
    private String annex = "";
    private String senderRegNumber = "";
    private String transmittalMode = "";
    private String party = "";
    private Boolean byRequestOnly = false;
    private String accessRestrictionChangeReason = "";
    private Date sendDate = null;
    private String uri = "";
    private String docTypeName = "";
    @JsonIgnore
    private Volume parentVolume;

    public Document() {}

    public Document(AtpDocument doc) {
        noderef = doc.getDokNr();
        regDateTime = doc.getRegistreerimiskuupaev(); //*
        regNumber = doc.getRegistreerimisnumber();
        title = doc.getPealkiri();
        accessRestriction = "Avalik".equalsIgnoreCase(doc.getJuurdepaasupiirang()) ? "Avalik" : "AK";
        accessRestrictionReason = doc.getJuurdepaasupiiranguAlus();
        accessRestrictionBeginDate = doc.getJuurdepaasupiiranguAlguskuupaev();
        accessRestrictionEndDate = doc.getJuurdepaasupiiranguLoppkuupaev();
        accessRestrictionEndDesc = doc.getJuurdepaasupiiranguLoppsyndmus();
        dueDate = doc.getTahtaeg();
        complianceDate = doc.getVastamiskuupaev();
        compilator = ""; //Kuvatakse "Vastutaja:"
        annex = ""; //Kuvatakse "Lisad:"
        senderRegNumber = doc.getSaatjaIndeks();
        transmittalMode = doc.getSaatmisviis();
        if (doc.getSuund().toLowerCase().startsWith("sisse")) {
            if (doc.getInfo() != null && doc.getInfo().trim().toLowerCase().startsWith("kellelt:")) {
                party = doc.getInfo().trim().substring(8);
                if (!"".equals(doc.getKellelt().trim()))
                    party += " (" + doc.getKellelt() + ")";
            } else {
                party = doc.getKellelt();
            }
        } else {
            if (doc.getInfo() != null && doc.getInfo().trim().toLowerCase().startsWith("kellele:")) {
                party = doc.getInfo().substring(8);
                if (!"".equals(doc.getKellele().trim()))
                    party += " (" + doc.getKellele() + ")";
            } else {
                party = doc.getKellele();
            }
        }
        byRequestOnly = false;
        accessRestrictionChangeReason = "";
        sendDate = doc.getVastamiskuupaev();
        uri = "";
        docTypeName = (doc.getLiik() + doc.getSuund()).replaceAll("\\.", "");
    }

    public String getVolumeNodeRef() {return this.volumeNodeRef;}
    public void setVolumeNodeRef(String volumeNodeRef) {this.volumeNodeRef = volumeNodeRef;}

    public String getNoderef() {return noderef;}
    public void setNoderef(String noderef) {this.noderef = noderef;}

    public Date getRegDateTime() {return regDateTime;}
    public void setRegDateTime(Date regDateTime) {this.regDateTime = regDateTime;}

    public String getRegNumber() {return regNumber;}
    public void setRegNumber(String regNumber) {this.regNumber = regNumber;}

    public String getTitle() {return title;}
    public void setTitle(String title) {this.title = title;}

    public String getAccessRestriction() {return accessRestriction;}
    public void setAccessRestriction(String accessRestriction) {
        this.accessRestriction = accessRestriction;
    }

    public String getAccessRestrictionReason() {return accessRestrictionReason;}
    public void setAccessRestrictionReason(String accessRestrictionReason) {
        this.accessRestrictionReason = accessRestrictionReason;
    }

    public Date getAccessRestrictionBeginDate() {return accessRestrictionBeginDate;}
    public void setAccessRestrictionBeginDate(Date accessRestrictionBeginDate) {
        this.accessRestrictionBeginDate = accessRestrictionBeginDate;
    }

    public Date getAccessRestrictionEndDate() {return accessRestrictionEndDate;}
    public void setAccessRestrictionEndDate(Date accessRestrictionEndDate) {
        this.accessRestrictionEndDate = accessRestrictionEndDate;
    }

    public String getAccessRestrictionEndDesc() {return accessRestrictionEndDesc;}
    public void setAccessRestrictionEndDesc(String accessRestrictionEndDesc) {
        this.accessRestrictionEndDesc = accessRestrictionEndDesc;
    }

    public Date getDueDate() {return dueDate;}
    public void setDueDate(Date dueDate) {this.dueDate = dueDate;}

    public Date getComplianceDate() {return complianceDate;}
    public void setComplianceDate(Date complianceDate){this.complianceDate = complianceDate;}

    public String getCompilator() {return compilator;}
    public void setCompilator(String compilator) {this.compilator = compilator;}

    public String getAnnex() {return annex;}
    public void setAnnex(String annex) {this.annex = annex;}

    public String getSenderRegNumber() {return senderRegNumber;}
    public void setSenderRegNumber(String senderRegNumber) {this.senderRegNumber = senderRegNumber;}

    public String getTransmittalMode() {return transmittalMode;}
    public void setTransmittalMode(String transmittalMode) {this.transmittalMode = transmittalMode;}

    public String getParty() {return party;}
    public void setParty(String party) {this.party = party;}

    public Boolean getByRequestOnly() {return byRequestOnly;}
    public void setByRequestOnly(Boolean byRequestOnly) {this.byRequestOnly = byRequestOnly;}

    public String getAccessRestrictionChangeReason() {return accessRestrictionChangeReason;}
    public void setAccessRestrictionChangeReason(String accessRestrictionChangeReason) {
        this.accessRestrictionChangeReason = accessRestrictionChangeReason;
    }

    public Date getSendDate() {return sendDate;}
    public void setSendDate(Date sendDate) {this.sendDate = sendDate;}

    public String getUri() {return uri;}
    public void setUri(String uri) {this.uri = uri;}

    public String getDocTypeName() {return docTypeName;}
    public void setDocTypeName(String docTypeName) {this.docTypeName = docTypeName;}

    public Volume getParentVolume() { return this.parentVolume; }
    public void setParentVolume(Volume v) {
        this.parentVolume = v;
        if (v != null)
            setVolumeNodeRef(v.getNoderef());
    }
}
