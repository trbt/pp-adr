package ee.upmind.adr.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.upmind.adr.models.pp.AtpDocument;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Series {
    private String mark = "-";
    private String title = "-";
    private Integer orderNum = 0;

    @JsonIgnore
    private Function parentFunction;

    @JsonIgnore
    public String getNoderef() {
        return getParentFunction().getMark() + " " + getParentFunction().getTitle() + "/" + getMark() + " " + getTitle();
    }

    @JsonIgnore
    public boolean isValid() {
        return !("".equals(mark) || "".equals(title));
    }

    public Series(AtpDocument doc) {
        //Sarja markeering indeksist kõik mis jääb enne kaldkriipsu
        if (doc.getIndeks() != null && !"".equals(doc.getIndeks())) {
            this.mark = doc.getIndeks().split("[\\/]")[0];
        }
        //TODO: Sarja nimetus, failist?
    }

    public boolean equals(Function other) {
        return (this.mark.equals(other.getMark())
                && this.title.equals(other.getTitle()));
    }

    public String getMark() {
        return mark;
    }
    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public Function getParentFunction() { return this.parentFunction; }
    public void setParentFunction(Function f) { this.parentFunction = f; }

    public Integer getOrderNum() {
        return orderNum;
    }
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }
}