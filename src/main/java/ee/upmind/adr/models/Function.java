package ee.upmind.adr.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.upmind.adr.models.pp.AtpDocument;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Function {
    private String mark = "-";
    private String title = "-";
    private Integer orderNum = 0;

    @JsonIgnore
    public String getNoderef() {
        return getMark() + " " + getTitle();
    }

    @JsonIgnore
    public boolean isValid() {
        return !("".equals(mark) || "".equals(title));
    }

    public Function() {}

    public Function(AtpDocument doc) {
        //Dokumendi funktsiooni nimetus „osakond“ väljast, funktsiooni markeering indeksist.
        if (doc.getOsakond() != null && !"".equals(doc.getOsakond())) {
            this.title = doc.getOsakond();
        }
        if (doc.getIndeks() != null && !"".equals(doc.getIndeks())) {
            this.mark = doc.getIndeks().split("-")[0];
        }
    }

    public boolean equals(Function other) {
        return (this.mark.equals(other.getMark())
                && this.title.equals(other.getTitle()));
    }

    public String getMark() { return mark; }
    public void setMark(String mark) { this.mark = mark; }

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public Integer getOrderNum() { return orderNum; }
    public void setOrderNum(Integer orderNum) { this.orderNum = orderNum; }

}
